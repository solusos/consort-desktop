using Peas;
using Cairo;
using GMenu;

public class PanelPlugin : ExtensionBase, Activatable {
    
    public Object object { owned get; construct; }
    
    Gtk.Window window;
    Gtk.Label clock;
    Gtk.MenuItem applications;
    Gtk.Menu app_menu;
    
    public PanelPlugin () {
        GLib.Object ();
    }
    
    protected void report_error (string message) {
        Gtk.MessageDialog msg = new Gtk.MessageDialog (window, Gtk.DialogFlags.DESTROY_WITH_PARENT, Gtk.MessageType.WARNING, Gtk.ButtonsType.CLOSE, message);
        msg.response.connect ( ()=> {
            msg.destroy();
        });
        msg.show ();
    }
    
    protected void menu_for_directory (ref Gtk.Menu base_menu, ref GMenu.TreeDirectory root) {
        TreeItemType next;
        TreeIter iter = root.iter ();
        
        while ((next = iter.next()) != TreeItemType.INVALID) {
            switch (next) {
                case TreeItemType.DIRECTORY:
                    var dir = iter.get_directory ();
                    var item = new Gtk.ImageMenuItem.with_label (dir.get_name());
                    item.always_show_image = true;
                    var img = new Gtk.Image.from_gicon (dir.get_icon (), Gtk.IconSize.MENU);
                    item.image = img;
                    Gtk.Menu submenu = new Gtk.Menu ();
                    item.set_submenu (submenu);
                    menu_for_directory (ref submenu, ref dir);
                    base_menu.append (item);
                    break;
                case TreeItemType.ENTRY:
                    var entry = iter.get_entry ();
                    var app_info = entry.get_app_info ();
                    var item = new Gtk.ImageMenuItem.with_label (app_info.get_name());
                    item.always_show_image = true;
                    var img = new Gtk.Image.from_gicon (app_info.get_icon (), Gtk.IconSize.MENU);
                    item.image = img;
                    base_menu.append (item);
                    item.activate.connect (()=> {
                        try {
                            if (! app_info.launch (null, null)) {
                                report_error ("Failed to launch application: %s".printf(app_info.get_commandline()));
                            }
                        } catch (GLib.Error e) { 
                            report_error ("Error when launching application: %s".printf(e.message));
                        } 
                    });
                    break;
                case TreeItemType.SEPARATOR:
                    //base_menu.append (new Gtk.SeparatorMenuItem());
                    break;
                default:
                    break;
            }
        }
    }
    
    protected void build_menus () {
        GMenu.Tree tree = new GMenu.Tree ("gnome-applications.menu", TreeFlags.NONE);
        tree.load_sync ();
        var root = tree.get_root_directory ();
        menu_for_directory (ref app_menu, ref root);
    }
    
    protected bool draw_panel (Context ctx) {
        // Semi-transparent black :)
        ctx.set_source_rgba(0.0,0.0,0.0,0.7);
        ctx.set_operator(Cairo.Operator.SOURCE);
        ctx.paint();
                
        return false;
    }
    
    protected bool update_timer () {
        var now = new DateTime.now_local ();
        var lab = "<big>%s</big>".printf (now.format("%H:%M:%S"));
        clock.set_markup (lab);
        return true;
    }
    
    public void activate () {
        var shell = object as Consort.Shell;
        window = new Gtk.Window ();
        window.set_decorated (false);
        
        //window.app_paintable = true;
        //window.draw.connect (draw_panel);
        
        window.realize ();
        shell.set_panel_window (window);
        
        // Main layout
        var hbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
        window.add (hbox);
        
        // Main menubar
        var mbar = new Gtk.MenuBar ();
        hbox.pack_start (mbar, false, false, 0);
        
        // Sample menu
        applications = new Gtk.MenuItem.with_label ("Applications");
        app_menu = new Gtk.Menu ();
        this.build_menus ();
        
        applications.set_submenu (app_menu);
        mbar.append (applications);
                
        // Make a simple clock.
        clock = new Gtk.Label ("");
        clock.use_markup = true;
        hbox.pack_end (clock, false, false, 0);
        
        this.update_timer ();
        
        var screen = window.get_screen ();
        var width = screen.width ();
        var height = 32;
        window.set_default_size (width, height);
        this.window.show_all (); 
        Timeout.add_seconds (1, update_timer);  
        
    }
    
    
    public void deactivate () {
        /* Clean up */
        this.window.set_visible (false);
        window.destroy ();
    }
    
    public void update_state () { }
}

public class PanelPluginConfig : ExtensionBase, PeasGtk.Configurable {
    
    public PanelPluginConfig () {
        GLib.Object ();
    }
    
    public Gtk.Widget create_configure_widget () {
        string text = "Configuration items pending";
        return new Gtk.Label (text);
    }
}

[ModuleInit]
public void peas_register_types (GLib.TypeModule module) {
    var objmodule = module as Peas.ObjectModule;
    objmodule.register_extension_type (typeof (Peas.Activatable), typeof (PanelPlugin));
    objmodule.register_extension_type (typeof (PeasGtk.Configurable), typeof (PanelPluginConfig));
}
